/*
 * Copyright (c) 2020 Miklós Máté
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * This is a prototype for a much larger system.
 *
 * To compile:
 *    gcc -g -O2 -std=gnu99 -Wall -Wextra -Werror reflections.c `pkg-config --cflags --libs sdl2` -lGL -lGLU -lGLEW -lm -o reflections
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <SDL.h>

#include <GL/glew.h>

#define ENABLE_ANIMATION 1
#define USE_GS 0

static const char *program_name = "Rendered Reflections";

static const unsigned INITIAL_WINDOW_WIDTH = 800;
static const unsigned INITIAL_WINDOW_HEIGHT = 600;

#define MAX_LIGHTS 4
#define MAX_BODIES 128

static const unsigned REFL_TEX_SIZE = 512;

static const unsigned cube_map_targets[] = {
    GL_TEXTURE_CUBE_MAP_POSITIVE_X,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
};

/***************************************/
/* Shader sources                      */

#if USE_GS

//TODO adding a GS is a major performance hit, can we do something about that?

static const char *vs_phong = R"(
#version 150 core

in vec3 Pos;
in vec3 Norm;
in vec3 Color;

out vec3 vNorm;
out vec3 vColor;

void main() {
    gl_Position = vec4(Pos, 1.0);

    vNorm = Norm;
    vColor = Color;
}
)";

//TODO right now this GS does the same thing as VS would do, but
//      it will allow us to do multi-layer drawing into the cubemap
static const char *gs_phong = R"(
#version 150 core

layout(triangles) in;

layout(triangle_strip, max_vertices=3) out;

in vec3 Pos[];
in vec3 vNorm[];
in vec3 vColor[];

out Data {
    vec3 Norm;
    vec3 Color;
    vec3 EyeDir;
    vec3 LightDir[4]; // MAX_LIGHTS
} dataOut;

// ModelView
uniform mat4 MV;

// Projection
uniform mat4 P;

// Scaling vector for transforming normals
uniform vec3 scale = vec3(1.0);

uniform vec4 lightPos_eye[4]; // MAX_LIGHTS
uniform int lightCount;

void main() {

    for (int i=0; i<gl_in.length(); i++) {
        vec4 pos_eye = MV * gl_in[i].gl_Position; // vertex in eye space
        gl_Position = P * pos_eye; // into clip space

        dataOut.Norm = mat3(MV) * (vNorm[i] / scale);
        dataOut.Color = vColor[i];
        dataOut.EyeDir = vec3(-pos_eye);

        for (int l=0; l<lightCount; l++) {
            dataOut.LightDir[l] = normalize(vec3(lightPos_eye[l] - pos_eye));
        }
        EmitVertex();
    }
    EndPrimitive();
}
)";

#else

static const char *vs_phong = R"(
#version 150 core

in vec3 Pos;
in vec3 Norm;
in vec3 Color;

out Data {
    vec3 Norm;
    vec3 Color;
    vec3 EyeDir;
    vec3 LightDir[4]; // MAX_LIGHTS
} dataOut;

// ModelView
uniform mat4 MV;

// Projection
uniform mat4 P;

// Scaling vector for transforming normals
uniform vec3 scale = vec3(1.0);

uniform vec4 lightPos_eye[4]; // MAX_LIGHTS
uniform int lightCount;

void main() {
    vec4 pos_eye = MV * vec4(Pos, 1.0); // vertex in eye space
    gl_Position = P * pos_eye; // into clip space

    dataOut.Norm = mat3(MV) * (Norm / scale);
    dataOut.Color = Color;
    dataOut.EyeDir = vec3(-pos_eye);

    for (int l=0; l<lightCount; l++) {
        dataOut.LightDir[l] = normalize(vec3(lightPos_eye[l] - pos_eye));
    }
}
)";

#endif

static const char *fs_phong = R"(
#version 150 core

out vec4 finalColor;

in Data {
    vec3 Norm;
    vec3 Color;
    vec3 EyeDir;
    vec3 LightDir[4]; // MAX_LIGHTS
} dataIn;

uniform vec3 bodyColor = vec3(1.0);

uniform vec3 lightColor[4]; // MAX_LIGHTS
uniform int lightCount;

void main() {
    // per-pixel phong shading
    vec3 n = normalize(dataIn.Norm);
    vec3 e = normalize(dataIn.EyeDir);
    finalColor = vec4(0.0);
    for (int i=0; i<lightCount; i++) {
        vec3 diff = vec3(0.0);
        vec3 spec = vec3(0.0);
        vec3 ambi = 0.2 * dataIn.Color * bodyColor * lightColor[i]; //TODO ambient factor from uniform

        vec3 l = normalize(dataIn.LightDir[i]);
        float d = dot(n, l);
        diff = step(0.0, d) * vec3(d) * dataIn.Color * bodyColor * lightColor[i];

        vec3 h = normalize(l + e); // half vector
        float specIntensity = max(dot(n, h), 0.0);
        spec = lightColor[i] * pow(specIntensity, 20); //TODO shininess from uniform

        finalColor += vec4(ambi+diff+spec, 1.0);
    }
}
)";

static const char *vs_sprite = R"(
#version 150 core

in vec4 Pos; // light position in eye space
in vec3 Color; // light color

uniform mat4 P;

out Data {
    vec3 Color;
} dataOut;

void main() {
    gl_Position = P * vec4(Pos);

    dataOut.Color = Color;
}
)";

static const char *fs_sprite = R"(
# version 150 core

out vec4 spriteColor;

in Data {
    vec3 Color;
} dataIn;

void main() {
    spriteColor = vec4(dataIn.Color, 1.0);
}
)";

static const char *vs_reflect = R"(
#version 150 core

in vec3 Pos;
in vec3 Norm;
in vec3 Color;

out Data {
    vec3 CubeCoord;
    vec3 PosEye;
    vec3 Norm;
} dataOut;

// ModelView
uniform mat4 MV;

// Projection
uniform mat4 P;

// Scaling vector for transforming normals
uniform vec3 scale = vec3(1.0);

// for reflection coordinates
uniform mat4 R;

void main() {
    gl_Position = P * MV * vec4(Pos, 1.0); // clip space

    vec3 pos_eye = vec3(MV * vec4(Pos, 1.0)); // eye space
    pos_eye = normalize(pos_eye);

    vec3 norm = normalize(mat3(MV) * (Norm / scale));

    vec3 f = reflect(pos_eye, norm);
    // transform it to match the eye space
    dataOut.CubeCoord = mat3(R)*f;

    dataOut.PosEye = pos_eye;
    dataOut.Norm = norm;
}
)";

static const char *fs_reflect = R"(
#version 150 core

out vec4 finalColor;

in Data {
    vec3 CubeCoord;
    vec3 PosEye;
    vec3 Norm;
} dataIn;

uniform vec3 bodyColor = vec3(1.0);

uniform samplerCube cube;

// for reflection coordinates
uniform mat4 R;

void main() {
    //TODO this is very distorted
    //finalColor = mix(vec4(bodyColor, 1.0), texture(cube, dataIn.CubeCoord), 0.799);

    //TODO this looks a bit better, but still not perfect (inaccuracies in the imported sphere data?)
    vec3 f = reflect(dataIn.PosEye, dataIn.Norm);
    vec3 coord = mat3(R)*f;
    finalColor = mix(vec4(bodyColor, 1.0), texture(cube, coord), 0.799); //TODO reflection factor from uniform
}
)";

/***************************************/
/* Vector, Matrix, Quaternion math {{{ */

typedef struct {
    GLfloat v[3];
} vec3;
typedef struct {
    GLfloat v[4];
} vec4;
typedef struct {
    GLfloat m[9];
} mat3;
typedef struct {
    GLfloat m[16];
} mat4;
typedef struct {
    vec3 v;
    GLfloat w;
} quat;

#define ASSIGN_VEC3(vec, x, y, z) (vec).v[0]=(x); (vec).v[1]=(y); (vec).v[2]=(z)
#define ASSIGN_VEC4(vec, x, y, z, w) (vec).v[0]=(x); (vec).v[1]=(y); (vec).v[2]=(z); (vec).v[3]=(w)

void vec3copy(vec3 *ret, const vec3 *v)
{
    ret->v[0] = v->v[0];
    ret->v[1] = v->v[1];
    ret->v[2] = v->v[2];
}

void vec3add(vec3 *ret, const vec3 *v1, const vec3 *v2)
{
    ret->v[0] = v1->v[0] + v2->v[0];
    ret->v[1] = v1->v[1] + v2->v[1];
    ret->v[2] = v1->v[2] + v2->v[2];
}

void vec3sub(vec3 *ret, const vec3 *v1, const vec3 *v2)
{
    ret->v[0] = v1->v[0] - v2->v[0];
    ret->v[1] = v1->v[1] - v2->v[1];
    ret->v[2] = v1->v[2] - v2->v[2];
}

GLfloat vec3len(const vec3 *v)
{
    return sqrt(v->v[0]*v->v[0] + v->v[1]*v->v[1] + v->v[2]*v->v[2]);
}

//TODO this shouldn't be in-place
void vec3scale(vec3 *v, GLfloat s)
{
    v->v[0] = v->v[0] * s;
    v->v[1] = v->v[1] * s;
    v->v[2] = v->v[2] * s;
}

//TODO this shouldn't be in-place
void vec3normalize(vec3 *v)
{
    vec3scale(v, 1.0/vec3len(v));
}

GLfloat vec3dot(const vec3 *v1, const vec3 *v2)
{
    return v1->v[0]*v2->v[0] + v1->v[1]*v2->v[1] + v1->v[2]*v2->v[2];
}

void vec3cross(vec3 *ret, const vec3 *v1, const vec3 *v2)
{
    ret->v[0] = v1->v[1]*v2->v[2] - v1->v[2]*v2->v[1];
    ret->v[1] = v1->v[2]*v2->v[0] - v1->v[0]*v2->v[2];
    ret->v[2] = v1->v[0]*v2->v[1] - v1->v[1]*v2->v[0];
}

// TODO same stuff for vec4

void vec4copy(vec4 *ret, const vec4 *v)
{
    ret->v[0] = v->v[0];
    ret->v[1] = v->v[1];
    ret->v[2] = v->v[2];
    ret->v[3] = v->v[3];
}

void vec4from3(vec4 *ret, const vec3 *v, const GLfloat s)
{
    ret->v[0] = v->v[0];
    ret->v[1] = v->v[1];
    ret->v[2] = v->v[2];
    ret->v[3] = s;
}

void vec3from4(vec3 *ret, const vec4 *v)
{
    ret->v[0] = v->v[0];
    ret->v[1] = v->v[1];
    ret->v[2] = v->v[2];
}

void vec3fprintf(FILE *fp, const vec3 *v, const char *name)
{
    fprintf(fp, "Vector3 '%s': (% .4f, % .4f, % .4f)\n", name, v->v[0], v->v[1], v->v[2]);
}

void vec4fprintf(FILE *fp, const vec4 *v, const char *name)
{
    fprintf(fp, "Vector4 '%s': (% .4f, % .4f, % .4f, % .4f)\n", name, v->v[0], v->v[1], v->v[2], v->v[3]);
}

void mat3identity(mat3 *ret)
{
    ret->m[0] = 1.0;
    ret->m[1] = 0.0;
    ret->m[2] = 0.0;
    ret->m[3] = 0.0;
    ret->m[4] = 1.0;
    ret->m[5] = 0.0;
    ret->m[6] = 0.0;
    ret->m[7] = 0.0;
    ret->m[8] = 1.0;
}

void mat4identity(mat4 *ret)
{
    ret->m[0]  = 1.0;
    ret->m[1]  = 0.0;
    ret->m[2]  = 0.0;
    ret->m[3]  = 0.0;
    ret->m[4]  = 0.0;
    ret->m[5]  = 1.0;
    ret->m[6]  = 0.0;
    ret->m[7]  = 0.0;
    ret->m[8]  = 0.0;
    ret->m[9]  = 0.0;
    ret->m[10] = 1.0;
    ret->m[11] = 0.0;
    ret->m[12] = 0.0;
    ret->m[13] = 0.0;
    ret->m[14] = 0.0;
    ret->m[15] = 1.0;
}

void mat4scaling(mat4 *ret, const vec3 *sc)
{
    ret->m[0]  = sc->v[0];
    ret->m[1]  = 0.0;
    ret->m[2]  = 0.0;
    ret->m[3]  = 0.0;
    ret->m[4]  = 0.0;
    ret->m[5]  = sc->v[1];
    ret->m[6]  = 0.0;
    ret->m[7]  = 0.0;
    ret->m[8]  = 0.0;
    ret->m[9]  = 0.0;
    ret->m[10] = sc->v[2];
    ret->m[11] = 0.0;
    ret->m[12] = 0.0;
    ret->m[13] = 0.0;
    ret->m[14] = 0.0;
    ret->m[15] = 1.0;
}

void mat4translating(mat4 *ret, const vec3 *tr)
{
    ret->m[0]  = 1.0;
    ret->m[1]  = 0.0;
    ret->m[2]  = 0.0;
    ret->m[3]  = 0.0;
    ret->m[4]  = 0.0;
    ret->m[5]  = 1.0;
    ret->m[6]  = 0.0;
    ret->m[7]  = 0.0;
    ret->m[8]  = 0.0;
    ret->m[9]  = 0.0;
    ret->m[10] = 1.0;
    ret->m[11] = 0.0;
    ret->m[12] = tr->v[0];
    ret->m[13] = tr->v[1];
    ret->m[14] = tr->v[2];
    ret->m[15] = 1.0;
}

void mat3copy(mat3 *ret, const mat3 *m)
{
    for (unsigned u=0; u<9; u++) {
        ret->m[u] = m->m[u];
    }
}

void mat4copy(mat4 *ret, const mat4 *m)
{
    for (unsigned u=0; u<16; u++) {
        ret->m[u] = m->m[u];
    }
}

void mat3mult(mat3 *ret, const mat3 *m1, const mat3 *m2)
{
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            ret->m[i+3*j] =
                m1->m[i]*m2->m[3*j] +
                m1->m[i+3]*m2->m[3*j+1] +
                m1->m[i+6]*m2->m[3*j+2];
        }
    }
}

void mat4mult(mat4 *ret, const mat4 *m1, const mat4 *m2)
{
    for (int i=0; i<4; i++) {
        for (int j=0; j<4; j++) {
            ret->m[i+4*j] =
                m1->m[i]*m2->m[4*j] +
                m1->m[i+4]*m2->m[4*j+1] +
                m1->m[i+8]*m2->m[4*j+2] +
                m1->m[i+12]*m2->m[4*j+3];
        }
    }
}

// optimization: the bottom row is always 0 0 0 1 in both input matrices
// NOT TRUE FOR PROJECTION MATRIX!
void mat4mult_opt(mat4 *ret, const mat4 *m1, const mat4 *m2)
{
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            ret->m[i+4*j] =
                m1->m[i]*m2->m[4*j] +
                m1->m[i+4]*m2->m[4*j+1] +
                m1->m[i+8]*m2->m[4*j+2];
        }
    }
    for (int i=0; i<3; i++) {
        ret->m[12+i] =
            m1->m[i]*m2->m[12] +
            m1->m[i+4]*m2->m[13] +
            m1->m[i+8]*m2->m[14] +
            m1->m[i+12]*1.0;
    }
    ret->m[3]  = 0.0;
    ret->m[7]  = 0.0;
    ret->m[11] = 0.0;
    ret->m[15] = 1.0;
}

void mat3vec3mult(vec3 *ret, const mat3 *m, const vec3 *v)
{
    for (int i=0; i<3; i++) {
        ret->v[i] =
            m->m[i]*v->v[0] +
            m->m[i+3]*v->v[1] +
            m->m[i+6]*v->v[2];
    }
}

void mat4vec4mult(vec4 *ret, const mat4 *m, const vec4 *v)
{
    for (int i=0; i<4; i++) {
        ret->v[i] =
            m->m[i]*v->v[0] +
            m->m[i+4]*v->v[1] +
            m->m[i+8]*v->v[2] +
            m->m[i+12]*v->v[3];
    }
}

GLfloat mat3det(const mat3 *m)
{
    return
        m->m[0] * (m->m[4]*m->m[8] - m->m[5]*m->m[7]) -
        m->m[1] * (m->m[3]*m->m[8] - m->m[5]*m->m[6]) +
        m->m[2] * (m->m[3]*m->m[7] - m->m[4]*m->m[6]);
}

GLfloat mat4det(void)
{
    //TODO what algo to use?
    return 0.0;
}

bool mat3invert(mat3 *ret, const mat3 *m)
{
    ret->m[0] = m->m[4]*m->m[8] - m->m[5]*m->m[7];
    ret->m[1] = m->m[2]*m->m[7] - m->m[1]*m->m[8];
    ret->m[2] = m->m[1]*m->m[5] - m->m[2]*m->m[4];
    ret->m[3] = m->m[5]*m->m[6] - m->m[3]*m->m[8];
    ret->m[4] = m->m[0]*m->m[8] - m->m[2]*m->m[6];
    ret->m[5] = m->m[2]*m->m[3] - m->m[0]*m->m[5];
    ret->m[6] = m->m[3]*m->m[7] - m->m[4]*m->m[6];
    ret->m[7] = m->m[1]*m->m[6] - m->m[0]*m->m[7];
    ret->m[8] = m->m[0]*m->m[4] - m->m[1]*m->m[3];

    GLfloat det = m->m[0]*ret->m[0] + m->m[1]*ret->m[3] + m->m[2]*ret->m[6];
    //fprintf(stderr, "mat3invert det %f mat3det %f\n", det, mat3det(m));
    if (fabs(det) < 0.000001) return false;
    for (int i=0; i<9; i++)
        ret->m[i] /= det;
    return true;
}

bool mat4invert(void)
{
    //TODO what algo to use?
    return false;
}

void mat3transpose(mat3 *ret, const mat3 *m)
{
    ret->m[0] = m->m[0];
    ret->m[1] = m->m[3];
    ret->m[2] = m->m[6];
    ret->m[3] = m->m[1];
    ret->m[4] = m->m[4];
    ret->m[5] = m->m[7];
    ret->m[6] = m->m[2];
    ret->m[7] = m->m[5];
    ret->m[8] = m->m[8];
}

void mat4transpose(mat4 *ret, const mat4 *m)
{
    ret->m[0]  = m->m[0];
    ret->m[1]  = m->m[4];
    ret->m[2]  = m->m[8];
    ret->m[3]  = m->m[12];
    ret->m[4]  = m->m[1];
    ret->m[5]  = m->m[5];
    ret->m[6]  = m->m[9];
    ret->m[7]  = m->m[13];
    ret->m[8]  = m->m[2];
    ret->m[9]  = m->m[6];
    ret->m[10] = m->m[10];
    ret->m[11] = m->m[14];
    ret->m[12] = m->m[3];
    ret->m[13] = m->m[7];
    ret->m[14] = m->m[11];
    ret->m[15] = m->m[15];
}

void mat4topleft3(mat3 *ret, const mat4 *m)
{
    ret->m[0] = m->m[0];
    ret->m[1] = m->m[1];
    ret->m[2] = m->m[2];
    ret->m[3] = m->m[4];
    ret->m[4] = m->m[5];
    ret->m[5] = m->m[6];
    ret->m[6] = m->m[8];
    ret->m[7] = m->m[9];
    ret->m[8] = m->m[10];
}

void mat3fprintf(FILE *fd, const mat3 *m, const char *name)
{
    fprintf(fd, "Matrix3 '%s':\n"
            "  / % .4f % .4f % .4f \\\n"
            "  | % .4f % .4f % .4f |\n"
            "  \\ % .4f % .4f % .4f /\n",
            name,
            m->m[0], m->m[3], m->m[6],
            m->m[1], m->m[4], m->m[7],
            m->m[2], m->m[5], m->m[8]);
}

void mat4fprintf(FILE *fd, const mat4 *m, const char *name)
{
    fprintf(fd, "Matrix4 '%s':\n"
            "  / % .4f % .4f % .4f % .4f \\\n"
            "  | % .4f % .4f % .4f % .4f |\n"
            "  | % .4f % .4f % .4f % .4f |\n"
            "  \\ % .4f % .4f % .4f % .4f /\n",
            name,
            m->m[0], m->m[4], m->m[8], m->m[12],
            m->m[1], m->m[5], m->m[9], m->m[13],
            m->m[2], m->m[6], m->m[10], m->m[14],
            m->m[3], m->m[7], m->m[11], m->m[15]);
}


void quat_from_axisangle(quat *ret, const vec3 *axis, const GLfloat angle)
{
    GLfloat sina = sin(angle / 2.0);
    GLfloat cosa = cos(angle / 2.0);
    vec3copy(&ret->v, axis);
    vec3normalize(&ret->v);
    vec3scale(&ret->v, sina);
    ret->w = cosa;
}

void quat_to_axisangle(vec3 *axis, GLfloat *angle, const quat *q)
{
    *angle = acos(q->w) * 2.0;
    GLfloat sina = sqrt(1.0 - q->w*q->w);
    //TODO if (sina) { this } else { return vec_ex }
    axis->v[0] = q->v.v[0] / sina;
    axis->v[1] = q->v.v[1] / sina;
    axis->v[2] = q->v.v[2] / sina;
    //TODO if not normalised: axis = q.v/magnitude(q.v); angle = 2atan(magnitude(q.v)/q.w)
}

void quat_copy(quat *ret, const quat *q)
{
    ret->w = q->w;
    vec3copy(&ret->v, &q->v);
}

void quat_conjugate(quat *ret, const quat *q)
{
    quat_copy(ret, q);
    vec3scale(&ret->v, -1.0);
}

GLfloat quat_magnitude(const quat *q)
{
    return sqrt(q->w*q->w + q->v.v[0]*q->v.v[0] + q->v.v[1]*q->v.v[1] + q->v.v[2]*q->v.v[2]);
}

//TODO this shouldn't be in-place
void quat_scale(quat *q, const GLfloat s)
{
    q->w *= s;
    q->v.v[0] *= s;
    q->v.v[1] *= s;
    q->v.v[2] *= s;
}

//TODO this shouldn't be in-place
void quat_normalise(quat *q)
{
    GLfloat m = quat_magnitude(q);
    q->w /= m;
    q->v.v[0] /= m;
    q->v.v[1] /= m;
    q->v.v[2] /= m;
}

void quat_inverse(quat *ret, const quat *q)
{
    GLfloat m = 1.0 / quat_magnitude(q);
    quat_conjugate(ret, q);
    quat_scale(ret, m);
}

void quat_mult(quat *ret, const quat *q1, const quat *q2)
{
    // Hamilton product
    ret->w = q1->w*q2->w - q1->v.v[0]*q2->v.v[0] - q1->v.v[1]*q2->v.v[1] - q1->v.v[2]*q2->v.v[2];
    ret->v.v[0] = q1->w*q2->v.v[0] + q1->v.v[0]*q2->w + q1->v.v[1]*q2->v.v[2] - q1->v.v[2]*q2->v.v[1];
    ret->v.v[1] = q1->w*q2->v.v[1] + q1->v.v[1]*q2->w + q1->v.v[2]*q2->v.v[0] - q1->v.v[0]*q2->v.v[2];
    ret->v.v[2] = q1->w*q2->v.v[2] + q1->v.v[2]*q2->w + q1->v.v[0]*q2->v.v[1] - q1->v.v[1]*q2->v.v[0];
}

void quat_to_rotmatrix(mat4 *ret, const quat *q)
{
    GLfloat s = 1.0; //TODO non-unit quaternion: s=1/quat_magnitude(q)^2
    ret->m[0]  = 1.0 - 2.0*s*(q->v.v[1]*q->v.v[1] + q->v.v[2]*q->v.v[2]);
    ret->m[1]  =       2.0*s*(q->v.v[0]*q->v.v[1] + q->v.v[2]*q->w);
    ret->m[2]  =       2.0*s*(q->v.v[0]*q->v.v[2] - q->v.v[1]*q->w);
    ret->m[3]  = 0.0;
    ret->m[4]  =       2.0*s*(q->v.v[0]*q->v.v[1] - q->v.v[2]*q->w);
    ret->m[5]  = 1.0 - 2.0*s*(q->v.v[0]*q->v.v[0] + q->v.v[2]*q->v.v[2]);
    ret->m[6]  =       2.0*s*(q->v.v[1]*q->v.v[2] + q->v.v[0]*q->w);
    ret->m[7]  = 0.0;
    ret->m[8]  =       2.0*s*(q->v.v[0]*q->v.v[2] + q->v.v[1]*q->w);
    ret->m[9]  =       2.0*s*(q->v.v[1]*q->v.v[2] - q->v.v[0]*q->w);
    ret->m[10] = 1.0 - 2.0*s*(q->v.v[0]*q->v.v[0] + q->v.v[1]*q->v.v[1]);
    ret->m[11] = 0.0;
    ret->m[12] = 0.0;
    ret->m[13] = 0.0;
    ret->m[14] = 0.0;
    ret->m[15] = 1.0;
}

void quat_from_rotmatrix(void/*quat *q, mat4 *rot*/)
{
    //TODO
}

//TODO quat_from_euler_angles() quat_to_euler_angles()

void quat_rotate_vec3(vec3 *ret, const quat *q, const vec3 *v)
{
    quat vq, retq;
    quat qi, tmp;

    quat_inverse(&qi, q);
    vq.w = 0.0;
    vec3copy(&vq.v, v);

    // p'=qpq^-1
    //TODO is this the correct multiplication order?
    quat_mult(&tmp, &vq, &qi);
    quat_mult(&retq, q, &tmp);

    vec3copy(ret, &retq.v);
}

void quat_rotate_vec4(void)
{
    //TODO
}

void quat_fprintf(FILE *fp, const quat *q, const char *name)
{
    fprintf(fp, "Quaternion '%s': % .4f + % .4fi + % .4fj + % .4fk\n", name,
            q->w, q->v.v[0], q->v.v[1], q->v.v[2]);
}

// }}}

/***************************************/
/* Viewport manipulation           {{{ */

void look_at(mat4 *ret, vec3 *center, vec3 *eye, vec3 *up)
{
    vec3 f, s, u;
    vec3sub(&f, center, eye);
    vec3normalize(&f);
    vec3cross(&s, &f, up);
    vec3normalize(&s);
    vec3cross(&u, &s, &f);

    ret->m[0]  = s.v[0];
    ret->m[1]  = u.v[0];
    ret->m[2]  = -f.v[0];
    ret->m[3]  = 0.0;
    ret->m[4]  = s.v[1];
    ret->m[5]  = u.v[1];
    ret->m[6]  = -f.v[1];
    ret->m[7]  = 0.0;
    ret->m[8]  = s.v[2];
    ret->m[9]  = u.v[2];
    ret->m[10] = -f.v[2];
    ret->m[11] = 0.0;
    ret->m[12] = -(eye->v[0]*s.v[0] + eye->v[1]*s.v[1] + eye->v[2]*s.v[2]);
    ret->m[13] = -(eye->v[0]*u.v[0] + eye->v[1]*u.v[1] + eye->v[2]*u.v[2]);
    ret->m[14] =  (eye->v[0]*f.v[0] + eye->v[1]*f.v[1] + eye->v[2]*f.v[2]);
    ret->m[15] = 1.0;
}

void frustum(mat4 *ret,
        GLfloat left, GLfloat right,
        GLfloat bottom, GLfloat top,
        GLfloat near, GLfloat far)
{
    ret->m[0]  = 2.0 * near / (right-left);
    ret->m[1]  = 0.0;
    ret->m[2]  = 0.0;
    ret->m[3]  = 0.0;
    ret->m[4]  = 0.0;
    ret->m[5]  = 2.0 * near / (top-bottom);
    ret->m[6]  = 0.0;
    ret->m[7]  = 0.0;
    ret->m[8]  = (right+left) / (right-left);
    ret->m[9]  = (top+bottom) / (top-bottom);
    ret->m[10] = (near+far) / (near-far);
    ret->m[11] = -1.0;
    ret->m[12] = 0.0;
    ret->m[13] = 0.0;
    ret->m[14] = 2.0*near*far / (near-far);
    ret->m[15] = 0.0;
}

void perspective(mat4 *ret,
        GLfloat fovy, GLfloat aspect,
        GLfloat near, GLfloat far)
{
    GLfloat f = 1.0/tan(fovy/2.0);

    ret->m[0]  = f / aspect;
    ret->m[1]  = 0.0;
    ret->m[2]  = 0.0;
    ret->m[3]  = 0.0;
    ret->m[4]  = 0.0;
    ret->m[5]  = f;
    ret->m[6]  = 0.0;
    ret->m[7]  = 0.0;
    ret->m[8]  = 0.0;
    ret->m[9]  = 0.0;
    ret->m[10] = (near+far) / (near-far);
    ret->m[11] = -1.0;
    ret->m[12] = 0.0;
    ret->m[13] = 0.0;
    ret->m[14] = 2.0*near*far / (near-far);
    ret->m[15] = 0.0;
}

// }}}

/***************************************/
/* Object structures                   */

struct Shader {
    GLuint prog;

    // all possible uniform locations in the shaders
    GLint mv_loc;
    GLint p_loc;
    GLint scale_loc;

    GLint lpos_loc;
    GLint lcolor_loc;
    GLint lcount_loc;

    GLint bcolor_loc;

    GLint sampler_loc;
    GLint r_loc;
};

struct Shape {
    GLuint vao;
    GLuint vbo;
    GLuint idx;
    unsigned indexcount;
};

struct Body;
typedef void (*body_move_fn)(struct Body *b, unsigned time, void *state);

struct Body {
    struct Shape *shape;
    vec3 size;
    vec3 center;
    quat rot;
    mat4 model;

    struct Shader *shader;
    struct Shader *shader_in_reflection;
    vec3 color; // final color = shape->vertex_color * body color
    GLuint reflection_texture;

    body_move_fn move;
    void *move_state;
};


typedef void (*light_move_fn)(vec3 *pos, unsigned time, void *state);

struct Lights {
    vec3 pos[MAX_LIGHTS];
    vec3 color[MAX_LIGHTS];
    light_move_fn move[MAX_LIGHTS];
    void *move_state[MAX_LIGHTS];
    unsigned count;
};

struct Camera {
    vec3 pos;
    vec3 lookat;
    vec3 up;
    mat4 view;

    GLfloat near;
    GLfloat far;
    GLfloat left;
    GLfloat right;
    GLfloat top;
    GLfloat bottom;
    mat4 proj;

    unsigned viewport_width;
    unsigned viewport_height;

    vec4 clearColor;

};

struct Scene {
    struct Body bodies[MAX_BODIES];
    struct Lights lights;
    unsigned bodycount;
    unsigned mirrorcount;

    struct Shader *phong;
    struct Shader *reflect;
    struct Shader *sprite;

    struct Shape *sprite_buffer;

    struct Camera reflection_cameras[6];
    GLuint reflection_fbo;
};


static void body_compute_modelmatrix(struct Body *b)
{
    quat_to_rotmatrix(&b->model, &b->rot);
    b->model.m[0]  *= b->size.v[0];
    b->model.m[1]  *= b->size.v[0];
    b->model.m[2]  *= b->size.v[0];
    b->model.m[4]  *= b->size.v[1];
    b->model.m[5]  *= b->size.v[1];
    b->model.m[6]  *= b->size.v[1];
    b->model.m[8]  *= b->size.v[2];
    b->model.m[9]  *= b->size.v[2];
    b->model.m[10] *= b->size.v[2];
    b->model.m[12] = b->center.v[0];
    b->model.m[13] = b->center.v[1];
    b->model.m[14] = b->center.v[2];
}

void body_apply_rotation(struct Body *b, vec3 *axis, GLfloat angle)
{
    quat quatb, quat2;
    quat_copy(&quatb, &b->rot);
    quat_from_axisangle(&quat2, axis, angle);
    quat_mult(&b->rot, &quat2, &quatb);
}

static void camera_update_view(struct Camera *camera)
{
    look_at(&camera->view, &camera->lookat, &camera->pos, &camera->up);
}

/***************************************/
/* GL error handling                   */

static GLenum checkGLerror(int line)
{
    GLenum error = GL_NO_ERROR;
#ifndef NDEBUG
    error = glGetError();
    if (error) {
        printf("line %d GL error %#.4x %s\n", line, error, gluErrorString(error));
    }
#endif
    return error;
}


/***************************************/
/* Initialization functions            */

struct VertData {
    GLfloat pos[3];
    GLfloat norm[3];
    GLubyte color[3];
};

static struct Shape *create_shape(const struct VertData *vert, const unsigned vert_count,
        const GLuint index[][3], const unsigned index_count)
{
    struct Shape *ret = malloc(sizeof(struct Shape));

    // the VAO holds the VBOs and the vertex attrib pointer infos
    glGenVertexArrays(1, &ret->vao);
    glBindVertexArray(ret->vao);

    glGenBuffers(1, &ret->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, ret->vbo);
    glBufferData(GL_ARRAY_BUFFER, vert_count, vert, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(struct VertData), (void*)(offsetof(struct VertData, pos)));
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(struct VertData), (void*)(offsetof(struct VertData, norm)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 3, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(struct VertData), (void*)(offsetof(struct VertData, color)));
    glEnableVertexAttribArray(2);

    glGenBuffers(1, &ret->idx);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ret->idx);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, index_count, index, GL_STATIC_DRAW);

    if (checkGLerror(__LINE__) != GL_NO_ERROR)
        exit(1);

    glBindVertexArray(0);

    ret->indexcount = index_count;

    return ret;
}

static struct Shape *create_cube()
{
    // cube geometry: separate color for each side {{{1
    static const struct VertData cube_vert[] = {
        // z=-1 plane, red
        {{-1.0, -1.0, -1.0}, { 0.0,  0.0, -1.0}, {255, 000, 000}},
        {{ 1.0, -1.0, -1.0}, { 0.0,  0.0, -1.0}, {255, 000, 000}},
        {{-1.0,  1.0, -1.0}, { 0.0,  0.0, -1.0}, {255, 000, 000}},
        {{ 1.0,  1.0, -1.0}, { 0.0,  0.0, -1.0}, {255, 000, 000}},
        // x=-1 plane, green
        {{-1.0, -1.0, -1.0}, {-1.0,  0.0,  0.0}, {000, 255, 000}},
        {{-1.0,  1.0, -1.0}, {-1.0,  0.0,  0.0}, {000, 255, 000}},
        {{-1.0, -1.0,  1.0}, {-1.0,  0.0,  0.0}, {000, 255, 000}},
        {{-1.0,  1.0,  1.0}, {-1.0,  0.0,  0.0}, {000, 255, 000}},
        // y=-1 plane, blue
        {{-1.0, -1.0, -1.0}, { 0.0, -1.0,  0.0}, {000, 000, 255}},
        {{ 1.0, -1.0, -1.0}, { 0.0, -1.0,  0.0}, {000, 000, 255}},
        {{-1.0, -1.0,  1.0}, { 0.0, -1.0,  0.0}, {000, 000, 255}},
        {{ 1.0, -1.0,  1.0}, { 0.0, -1.0,  0.0}, {000, 000, 255}},
        // z= 1 plane, yellow
        {{-1.0, -1.0,  1.0}, { 0.0,  0.0,  1.0}, {255, 255, 000}},
        {{ 1.0, -1.0,  1.0}, { 0.0,  0.0,  1.0}, {255, 255, 000}},
        {{-1.0,  1.0,  1.0}, { 0.0,  0.0,  1.0}, {255, 255, 000}},
        {{ 1.0,  1.0,  1.0}, { 0.0,  0.0,  1.0}, {255, 255, 000}},
        // x= 1 plane, cyan
        {{ 1.0, -1.0, -1.0}, { 1.0,  0.0,  0.0}, {000, 255, 255}},
        {{ 1.0,  1.0, -1.0}, { 1.0,  0.0,  0.0}, {000, 255, 255}},
        {{ 1.0, -1.0,  1.0}, { 1.0,  0.0,  0.0}, {000, 255, 255}},
        {{ 1.0,  1.0,  1.0}, { 1.0,  0.0,  0.0}, {000, 255, 255}},
        // y= 1 plane, magenta
        {{-1.0,  1.0, -1.0}, { 0.0,  1.0,  0.0}, {255, 000, 255}},
        {{ 1.0,  1.0, -1.0}, { 0.0,  1.0,  0.0}, {255, 000, 255}},
        {{-1.0,  1.0,  1.0}, { 0.0,  1.0,  0.0}, {255, 000, 255}},
        {{ 1.0,  1.0,  1.0}, { 0.0,  1.0,  0.0}, {255, 000, 255}},
    };

    static const GLuint cube_index[][3] = {
        { 2,  1,  0}, { 3,  1,  2},
        { 6,  5,  4}, { 7,  5,  6},
        { 8,  9, 10}, {10,  9, 11},
        {12, 13, 14}, {14, 13, 15},
        {16, 17, 18}, {18, 17, 19},
        {22, 21, 20}, {23, 21, 22},
    }; // }}}

    printf("Cube vertices %ld indices %ld\n", sizeof(cube_vert)/sizeof(cube_vert[0]), sizeof(cube_index)/sizeof(cube_index[0]));

    return create_shape(cube_vert, sizeof(cube_vert), cube_index, sizeof(cube_index));
}

static struct Shape *create_sphere()
{
    // sphere geometry: icosahedron subdivided 2x along the edges {{{1
    // vertex color is all white
    static const struct VertData sphere_vert[] = {
        {{ 0.525731,  0.000000,  0.850651}, { 0.525731,  0.000000,  0.850651}, {255, 255, 255}},
        {{ 0.000000,  0.850651,  0.525731}, { 0.000000,  0.850651,  0.525731}, {255, 255, 255}},
        {{-0.525731,  0.000000,  0.850651}, {-0.525731,  0.000000,  0.850651}, {255, 255, 255}},
        {{-0.850651,  0.525731,  0.000000}, {-0.850651,  0.525731,  0.000000}, {255, 255, 255}},
        {{ 0.000000,  0.850651, -0.525731}, { 0.000000,  0.850651, -0.525731}, {255, 255, 255}},
        {{-0.850651,  0.525731,  0.000000}, {-0.850651,  0.525731,  0.000000}, {255, 255, 255}},
        {{ 0.850651,  0.525731,  0.000000}, { 0.850651,  0.525731,  0.000000}, {255, 255, 255}},
        {{ 0.850651,  0.525731,  0.000000}, { 0.850651,  0.525731,  0.000000}, {255, 255, 255}},
        {{ 0.850651, -0.525731,  0.000000}, { 0.850651, -0.525731,  0.000000}, {255, 255, 255}},
        {{ 0.850651, -0.525731,  0.000000}, { 0.850651, -0.525731,  0.000000}, {255, 255, 255}},
        {{ 0.525731,  0.000000, -0.850651}, { 0.525731,  0.000000, -0.850651}, {255, 255, 255}},
        {{-0.525731,  0.000000, -0.850651}, {-0.525731,  0.000000, -0.850651}, {255, 255, 255}},
        {{ 0.000000, -0.850651, -0.525731}, { 0.000000, -0.850651, -0.525731}, {255, 255, 255}},
        {{ 0.000000, -0.850651,  0.525731}, { 0.000000, -0.850651,  0.525731}, {255, 255, 255}},
        {{-0.850651, -0.525731,  0.000000}, {-0.850651, -0.525731,  0.000000}, {255, 255, 255}},
        {{-0.850651, -0.525731,  0.000000}, {-0.850651, -0.525731,  0.000000}, {255, 255, 255}},
        {{ 0.403548,  0.326477,  0.854729}, { 0.403548,  0.326477,  0.854729}, {255, 255, 255}},
        {{ 0.201774,  0.000000,  0.979432}, { 0.201774,  0.000000,  0.979432}, {255, 255, 255}},
        {{-0.000000,  0.356822,  0.934172}, {-0.000000,  0.356822,  0.934172}, {255, 255, 255}},
        {{-0.201774,  0.000000,  0.979432}, {-0.201774,  0.000000,  0.979432}, {255, 255, 255}},
        {{-0.403548,  0.326477,  0.854729}, {-0.403548,  0.326477,  0.854729}, {255, 255, 255}},
        {{ 0.201774,  0.652955,  0.730026}, { 0.201774,  0.652955,  0.730026}, {255, 255, 255}},
        {{-0.201774,  0.652955,  0.730026}, {-0.201774,  0.652955,  0.730026}, {255, 255, 255}},
        {{-0.326477,  0.854729,  0.403548}, {-0.326477,  0.854729,  0.403548}, {255, 255, 255}},
        {{-0.577350,  0.577350,  0.577350}, {-0.577350,  0.577350,  0.577350}, {255, 255, 255}},
        {{-0.730026,  0.201774,  0.652955}, {-0.730026,  0.201774,  0.652955}, {255, 255, 255}},
        {{-0.652955,  0.730026,  0.201774}, {-0.652955,  0.730026,  0.201774}, {255, 255, 255}},
        {{-0.854729,  0.403548,  0.326477}, {-0.854729,  0.403548,  0.326477}, {255, 255, 255}},
        {{ 0.000000,  0.979432,  0.201774}, { 0.000000,  0.979432,  0.201774}, {255, 255, 255}},
        {{-0.356822,  0.934172, -0.000000}, {-0.356822,  0.934172, -0.000000}, {255, 255, 255}},
        {{-0.652955,  0.730026,  0.201774}, {-0.652955,  0.730026,  0.201774}, {255, 255, 255}},
        {{-0.652955,  0.730026, -0.201774}, {-0.652955,  0.730026, -0.201774}, {255, 255, 255}},
        {{ 0.000000,  0.979432, -0.201774}, { 0.000000,  0.979432, -0.201774}, {255, 255, 255}},
        {{-0.356822,  0.934172, -0.000000}, {-0.356822,  0.934172, -0.000000}, {255, 255, 255}},
        {{-0.326477,  0.854729, -0.403548}, {-0.326477,  0.854729, -0.403548}, {255, 255, 255}},
        {{ 0.652955,  0.730026, -0.201774}, { 0.652955,  0.730026, -0.201774}, {255, 255, 255}},
        {{ 0.652955,  0.730026,  0.201774}, { 0.652955,  0.730026,  0.201774}, {255, 255, 255}},
        {{ 0.652955,  0.730026,  0.201774}, { 0.652955,  0.730026,  0.201774}, {255, 255, 255}},
        {{ 0.356822,  0.934172,  0.000000}, { 0.356822,  0.934172,  0.000000}, {255, 255, 255}},
        {{ 0.326477,  0.854729,  0.403548}, { 0.326477,  0.854729,  0.403548}, {255, 255, 255}},
        {{ 0.326477,  0.854729, -0.403548}, { 0.326477,  0.854729, -0.403548}, {255, 255, 255}},
        {{ 0.356822,  0.934172,  0.000000}, { 0.356822,  0.934172,  0.000000}, {255, 255, 255}},
        {{ 0.730026,  0.201774,  0.652955}, { 0.730026,  0.201774,  0.652955}, {255, 255, 255}},
        {{ 0.577350,  0.577350,  0.577350}, { 0.577350,  0.577350,  0.577350}, {255, 255, 255}},
        {{ 0.854729,  0.403548,  0.326477}, { 0.854729,  0.403548,  0.326477}, {255, 255, 255}},
        {{ 0.730026, -0.201774,  0.652955}, { 0.730026, -0.201774,  0.652955}, {255, 255, 255}},
        {{ 0.934172,  0.000000,  0.356822}, { 0.934172,  0.000000,  0.356822}, {255, 255, 255}},
        {{ 0.979432,  0.201774, -0.000000}, { 0.979432,  0.201774, -0.000000}, {255, 255, 255}},
        {{ 0.854729, -0.403548,  0.326477}, { 0.854729, -0.403548,  0.326477}, {255, 255, 255}},
        {{ 0.979432, -0.201774, -0.000000}, { 0.979432, -0.201774, -0.000000}, {255, 255, 255}},
        {{ 0.854729, -0.403548, -0.326477}, { 0.854729, -0.403548, -0.326477}, {255, 255, 255}},
        {{ 0.979432, -0.201774,  0.000000}, { 0.979432, -0.201774,  0.000000}, {255, 255, 255}},
        {{ 0.934172,  0.000000, -0.356822}, { 0.934172,  0.000000, -0.356822}, {255, 255, 255}},
        {{ 0.979432,  0.201774,  0.000000}, { 0.979432,  0.201774,  0.000000}, {255, 255, 255}},
        {{ 0.854729,  0.403548, -0.326477}, { 0.854729,  0.403548, -0.326477}, {255, 255, 255}},
        {{ 0.730026, -0.201774, -0.652955}, { 0.730026, -0.201774, -0.652955}, {255, 255, 255}},
        {{ 0.730026,  0.201774, -0.652955}, { 0.730026,  0.201774, -0.652955}, {255, 255, 255}},
        {{ 0.577350,  0.577350, -0.577350}, { 0.577350,  0.577350, -0.577350}, {255, 255, 255}},
        {{ 0.201774,  0.652955, -0.730026}, { 0.201774,  0.652955, -0.730026}, {255, 255, 255}},
        {{ 0.403548,  0.326477, -0.854729}, { 0.403548,  0.326477, -0.854729}, {255, 255, 255}},
        {{ 0.201774,  0.000000, -0.979432}, { 0.201774,  0.000000, -0.979432}, {255, 255, 255}},
        {{-0.000000,  0.356822, -0.934172}, {-0.000000,  0.356822, -0.934172}, {255, 255, 255}},
        {{-0.201774,  0.652955, -0.730026}, {-0.201774,  0.652955, -0.730026}, {255, 255, 255}},
        {{-0.201774,  0.000000, -0.979432}, {-0.201774,  0.000000, -0.979432}, {255, 255, 255}},
        {{-0.403548,  0.326477, -0.854729}, {-0.403548,  0.326477, -0.854729}, {255, 255, 255}},
        {{ 0.403548, -0.326477, -0.854729}, { 0.403548, -0.326477, -0.854729}, {255, 255, 255}},
        {{-0.000000, -0.356822, -0.934172}, {-0.000000, -0.356822, -0.934172}, {255, 255, 255}},
        {{-0.403548, -0.326477, -0.854729}, {-0.403548, -0.326477, -0.854729}, {255, 255, 255}},
        {{ 0.201774, -0.652955, -0.730026}, { 0.201774, -0.652955, -0.730026}, {255, 255, 255}},
        {{-0.201774, -0.652955, -0.730026}, {-0.201774, -0.652955, -0.730026}, {255, 255, 255}},
        {{ 0.577350, -0.577350, -0.577350}, { 0.577350, -0.577350, -0.577350}, {255, 255, 255}},
        {{ 0.326477, -0.854729, -0.403548}, { 0.326477, -0.854729, -0.403548}, {255, 255, 255}},
        {{ 0.652955, -0.730026, -0.201774}, { 0.652955, -0.730026, -0.201774}, {255, 255, 255}},
        {{ 0.652955, -0.730026,  0.201774}, { 0.652955, -0.730026,  0.201774}, {255, 255, 255}},
        {{ 0.652955, -0.730026, -0.201774}, { 0.652955, -0.730026, -0.201774}, {255, 255, 255}},
        {{ 0.356822, -0.934172,  0.000000}, { 0.356822, -0.934172,  0.000000}, {255, 255, 255}},
        {{-0.000000, -0.979432, -0.201774}, {-0.000000, -0.979432, -0.201774}, {255, 255, 255}},
        {{ 0.326477, -0.854729,  0.403548}, { 0.326477, -0.854729,  0.403548}, {255, 255, 255}},
        {{ 0.356822, -0.934172,  0.000000}, { 0.356822, -0.934172,  0.000000}, {255, 255, 255}},
        {{-0.000000, -0.979432,  0.201774}, {-0.000000, -0.979432,  0.201774}, {255, 255, 255}},
        {{-0.326477, -0.854729,  0.403548}, {-0.326477, -0.854729,  0.403548}, {255, 255, 255}},
        {{-0.356822, -0.934172, -0.000000}, {-0.356822, -0.934172, -0.000000}, {255, 255, 255}},
        {{-0.326477, -0.854729, -0.403548}, {-0.326477, -0.854729, -0.403548}, {255, 255, 255}},
        {{-0.652955, -0.730026,  0.201774}, {-0.652955, -0.730026,  0.201774}, {255, 255, 255}},
        {{-0.356822, -0.934172, -0.000000}, {-0.356822, -0.934172, -0.000000}, {255, 255, 255}},
        {{-0.652955, -0.730026, -0.201774}, {-0.652955, -0.730026, -0.201774}, {255, 255, 255}},
        {{-0.652955, -0.730026, -0.201774}, {-0.652955, -0.730026, -0.201774}, {255, 255, 255}},
        {{-0.652955, -0.730026,  0.201774}, {-0.652955, -0.730026,  0.201774}, {255, 255, 255}},
        {{-0.201774, -0.652955,  0.730026}, {-0.201774, -0.652955,  0.730026}, {255, 255, 255}},
        {{-0.577350, -0.577350,  0.577350}, {-0.577350, -0.577350,  0.577350}, {255, 255, 255}},
        {{-0.854729, -0.403548,  0.326477}, {-0.854729, -0.403548,  0.326477}, {255, 255, 255}},
        {{-0.403548, -0.326477,  0.854729}, {-0.403548, -0.326477,  0.854729}, {255, 255, 255}},
        {{-0.730026, -0.201774,  0.652955}, {-0.730026, -0.201774,  0.652955}, {255, 255, 255}},
        {{ 0.201774, -0.652955,  0.730026}, { 0.201774, -0.652955,  0.730026}, {255, 255, 255}},
        {{ 0.000000, -0.356822,  0.934172}, { 0.000000, -0.356822,  0.934172}, {255, 255, 255}},
        {{ 0.403548, -0.326477,  0.854729}, { 0.403548, -0.326477,  0.854729}, {255, 255, 255}},
        {{ 0.577350, -0.577350,  0.577350}, { 0.577350, -0.577350,  0.577350}, {255, 255, 255}},
        {{-0.979432, -0.201774,  0.000000}, {-0.979432, -0.201774,  0.000000}, {255, 255, 255}},
        {{-0.934172,  0.000000,  0.356822}, {-0.934172,  0.000000,  0.356822}, {255, 255, 255}},
        {{-0.979432,  0.201774,  0.000000}, {-0.979432,  0.201774,  0.000000}, {255, 255, 255}},
        {{-0.730026, -0.201774, -0.652955}, {-0.730026, -0.201774, -0.652955}, {255, 255, 255}},
        {{-0.730026,  0.201774, -0.652955}, {-0.730026,  0.201774, -0.652955}, {255, 255, 255}},
        {{-0.934172,  0.000000, -0.356822}, {-0.934172,  0.000000, -0.356822}, {255, 255, 255}},
        {{-0.854729,  0.403548, -0.326477}, {-0.854729,  0.403548, -0.326477}, {255, 255, 255}},
        {{-0.979432,  0.201774,  0.000000}, {-0.979432,  0.201774,  0.000000}, {255, 255, 255}},
        {{-0.854729, -0.403548, -0.326477}, {-0.854729, -0.403548, -0.326477}, {255, 255, 255}},
        {{-0.979432, -0.201774,  0.000000}, {-0.979432, -0.201774,  0.000000}, {255, 255, 255}},
        {{-0.577350,  0.577350, -0.577350}, {-0.577350,  0.577350, -0.577350}, {255, 255, 255}},
        {{-0.577350, -0.577350, -0.577350}, {-0.577350, -0.577350, -0.577350}, {255, 255, 255}},
    };

    static const GLuint sphere_index[][3] = {
        {  0,  16,  17}, { 17,  18,  19}, { 19,  20,   2}, { 16,  21,  18},
        { 18,  22,  20}, { 21,   1,  22}, { 16,  18,  17}, { 18,  20,  19},
        { 21,  22,  18}, {  1,  23,  22}, { 22,  24,  20}, { 20,  25,   2},
        { 23,  26,  24}, { 24,  27,  25}, { 26,   3,  27}, { 23,  24,  22},
        { 24,  25,  20}, { 26,  27,  24}, {  1,  28,  23}, { 23,  29,  26},
        { 30,  31,   5}, { 28,  32,  33}, { 33,  34,  31}, { 32,   4,  34},
        { 28,  29,  23}, { 33,  31,  30}, { 32,  34,  33}, {  6,  35,  36},
        { 37,  38,  39}, { 39,  28,   1}, { 35,  40,  41}, { 41,  32,  28},
        { 40,   4,  32}, { 35,  41,  36}, { 38,  28,  39}, { 40,  32,  41},
        {  0,  42,  16}, { 16,  43,  21}, { 21,  39,   1}, { 42,  44,  43},
        { 43,  37,  39}, { 44,   7,  37}, { 42,  43,  16}, { 43,  39,  21},
        { 44,  37,  43}, {  0,  45,  42}, { 42,  46,  44}, { 44,  47,   7},
        { 45,  48,  46}, { 46,  49,  47}, { 48,   8,  49}, { 45,  46,  42},
        { 46,  47,  44}, { 48,  49,  46}, {  9,  50,  51}, { 51,  52,  53},
        { 53,  54,   6}, { 50,  55,  52}, { 52,  56,  54}, { 55,  10,  56},
        { 50,  52,  51}, { 52,  54,  53}, { 55,  56,  52}, {  6,  54,  35},
        { 35,  57,  40}, { 40,  58,   4}, { 54,  56,  57}, { 57,  59,  58},
        { 56,  10,  59}, { 54,  57,  35}, { 57,  58,  40}, { 56,  59,  57},
        { 10,  60,  59}, { 59,  61,  58}, { 58,  62,   4}, { 60,  63,  61},
        { 61,  64,  62}, { 63,  11,  64}, { 60,  61,  59}, { 61,  62,  58},
        { 63,  64,  61}, { 10,  65,  60}, { 60,  66,  63}, { 63,  67,  11},
        { 65,  68,  66}, { 66,  69,  67}, { 68,  12,  69}, { 65,  66,  60},
        { 66,  67,  63}, { 68,  69,  66}, { 10,  55,  65}, { 65,  70,  68},
        { 68,  71,  12}, { 55,  50,  70}, { 70,  72,  71}, { 50,   9,  72},
        { 55,  70,  65}, { 70,  71,  68}, { 50,  72,  70}, {  8,  73,  74},
        { 72,  75,  71}, { 71,  76,  12}, { 73,  77,  78}, { 78,  79,  76},
        { 77,  13,  79}, { 73,  78,  74}, { 75,  76,  71}, { 77,  79,  78},
        { 13,  80,  79}, { 79,  81,  76}, { 76,  82,  12}, { 80,  83,  81},
        { 84,  85,  82}, { 83,  14,  86}, { 80,  81,  79}, { 84,  82,  76},
        { 87,  85,  84}, { 13,  88,  80}, { 80,  89,  83}, { 83,  90,  14},
        { 88,  91,  89}, { 89,  92,  90}, { 91,   2,  92}, { 88,  89,  80},
        { 89,  90,  83}, { 91,  92,  89}, { 13,  93,  88}, { 88,  94,  91},
        { 91,  19,   2}, { 93,  95,  94}, { 94,  17,  19}, { 95,   0,  17},
        { 93,  94,  88}, { 94,  19,  91}, { 95,  17,  94}, {  8,  48,  73},
        { 73,  96,  77}, { 77,  93,  13}, { 48,  45,  96}, { 96,  95,  93},
        { 45,   0,  95}, { 48,  96,  73}, { 96,  93,  77}, { 45,  95,  96},
        { 14,  90,  97}, { 97,  98,  99}, { 99,  27,   3}, { 90,  92,  98},
        { 98,  25,  27}, { 92,   2,  25}, { 90,  98,  97}, { 98,  27,  99},
        { 92,  25,  98}, { 11, 100, 101}, {101, 102, 103}, {103, 104,   5},
        {100, 105, 102}, {102, 106, 104}, {105,  15, 106}, {100, 102, 101},
        {102, 104, 103}, {105, 106, 102}, {  4,  62,  34}, { 34, 107,  31},
        { 31, 103,   5}, { 62,  64, 107}, {107, 101, 103}, { 64,  11, 101},
        { 62, 107,  34}, {107, 103,  31}, { 64, 101, 107}, { 15, 105,  85},
        { 85, 108,  82}, { 82,  69,  12}, {105, 100, 108}, {108,  67,  69},
        {100,  11,  67}, {105, 108,  85}, {108,  69,  82}, {100,  67, 108},
    }; // }}}

    printf("Sphere vertices %ld indices %ld\n", sizeof(sphere_vert)/sizeof(sphere_vert[0]), sizeof(sphere_index)/sizeof(sphere_index[0]));

    return create_shape(sphere_vert, sizeof(sphere_vert), sphere_index, sizeof(sphere_index));
}

static GLuint compile_shader(GLenum type, const char *name, const char *source)
{
    GLuint shad = glCreateShader(type);
    glShaderSource(shad, 1, &source, NULL);
    glCompileShader(shad);

    GLint status;
    glGetShaderiv(shad, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
        char log[1024];
        GLsizei length;
        glGetShaderInfoLog(shad, 1023, &length, log);
        const char *typename = "unknown";
        switch (type) {
            case GL_VERTEX_SHADER: typename = "vertex"; break;
            case GL_GEOMETRY_SHADER: typename = "geometry"; break;
            case GL_FRAGMENT_SHADER: typename = "fragment"; break;
        }
        fprintf(stderr, "compilation of %s shader '%s' failed: '%s'\n",
                typename, name, log);
        return 0;
    }

    return shad;
}

static struct Shader *create_shader_program(const char *name, const char *vs_source, const char *gs_source, const char *fs_source)
{
    static const char *vertex_attrib_locations[] = {"Pos", "Norm", "Color", NULL};

    GLuint vs = compile_shader(GL_VERTEX_SHADER, name, vs_source);
    GLuint fs = compile_shader(GL_FRAGMENT_SHADER, name, fs_source);
    if (vs == 0 || fs == 0) {
        return NULL;
    }
    GLuint gs;
    if (gs_source) {
        gs = compile_shader(GL_GEOMETRY_SHADER, name, gs_source);
        if (gs == 0) {
            return NULL;
        }
    }

    struct Shader *ret = malloc(sizeof(struct Shader));
    ret->prog = glCreateProgram();
    glAttachShader(ret->prog, vs);
    glAttachShader(ret->prog, fs);
    if (gs_source)
        glAttachShader(ret->prog, gs);

    //TODO use explicit attrib binding from GL3.3?
    for (int i=0; vertex_attrib_locations[i]; i++)
        glBindAttribLocation(ret->prog, i, vertex_attrib_locations[i]);

    glLinkProgram(ret->prog);

    GLint status;
    glGetProgramiv(ret->prog, GL_LINK_STATUS, &status);
    if (status == GL_FALSE) {
        char log[1024];
        GLsizei length;
        glGetProgramInfoLog(ret->prog, 1023, &length, log);
        fprintf(stderr, "linking of program '%s' failed: '%s'\n", name, log);
        return NULL;
    }

    // these are refcounted, when program is deleted, they automatically go with it
    glDeleteShader(vs);
    glDeleteShader(fs);

    ret->mv_loc = glGetUniformLocation(ret->prog, "MV");
    ret->p_loc = glGetUniformLocation(ret->prog, "P");
    ret->scale_loc = glGetUniformLocation(ret->prog, "scale");

    ret->lpos_loc = glGetUniformLocation(ret->prog, "lightPos_eye");
    ret->lcolor_loc = glGetUniformLocation(ret->prog, "lightColor");
    ret->lcount_loc = glGetUniformLocation(ret->prog, "lightCount");

    ret->bcolor_loc = glGetUniformLocation(ret->prog, "bodyColor");

    ret->sampler_loc = glGetUniformLocation(ret->prog, "cube");
    ret->r_loc = glGetUniformLocation(ret->prog, "R");

    int posl = glGetAttribLocation(ret->prog, "Pos");
    int norml = glGetAttribLocation(ret->prog, "Norm");
    int colorl = glGetAttribLocation(ret->prog, "Color");
    printf("Shader '%s' vertex attrib locations: pos %d norm %d color %d\n", name, posl, norml, colorl);

    printf("Shader '%s' uniforms mv %d p %d scale %d lpos %d lcolor %d lcount %d bcolor %d sampler %d r %d\n",
            name,
            ret->mv_loc, ret->p_loc, ret->scale_loc,
            ret->lpos_loc, ret->lcolor_loc, ret->lcount_loc,
            ret->bcolor_loc, ret->sampler_loc, ret->r_loc);

    return ret;
}

//TODO static void delete_program(GLuint prog) { glDeleteProgram(prog); }

static void init_shaders(struct Scene *scene)
{
#if USE_GS
    //TODO this is very slow
    scene->phong = create_shader_program("Phong", vs_phong, gs_phong, fs_phong);
#else
    scene->phong = create_shader_program("Phong", vs_phong, NULL, fs_phong);
#endif
    if (!scene->phong) {
        exit(1);
    }

    scene->reflect = create_shader_program("Reflect", vs_reflect, NULL, fs_reflect);
    if (!scene->reflect) {
        exit(1);
    }

    scene->sprite = create_shader_program("Sprite", vs_sprite, NULL, fs_sprite);
    if (!scene->sprite) {
        exit(1);
    }
}

static void sphere_color(vec3 *ret, unsigned sp, unsigned num)
{
    // this is like a HSV to RGB mapping with max saturation and value
    float h = (float)sp/(float)num;
    int f = h*6.0;
    float hf = (h - 1.0/6.0*f)*6.0;

    switch (f) {
        case 0:
            ASSIGN_VEC3(*ret, 1.0, hf, 0.0); break;
        case 1:
            ASSIGN_VEC3(*ret, 1.0-hf, 1.0, 0.0); break;
        case 2:
            ASSIGN_VEC3(*ret, 0.0, 1.0, hf); break;
        case 3:
            ASSIGN_VEC3(*ret, 0.0, 1.0-hf, 1.0); break;
        case 4:
            ASSIGN_VEC3(*ret, hf, 0.0, 1.0); break;
        case 5:
            ASSIGN_VEC3(*ret, 1.0, 0.0, 1.0-hf); break;
        default:
            ASSIGN_VEC3(*ret, 1.0, 1.0, 1.0);
    }
}

struct MoveMirrorState {
    // rotation state
    vec3 axis;
    GLfloat rotation;
    quat rot;

    unsigned last_time;
};
static void move_mirror(struct Body *b, unsigned time, void *s)
{
    struct MoveMirrorState *mms = s;
    if (time != mms->last_time) {
        // update mms->rot
        quat copyrot;
        quat_copy(&copyrot, &mms->rot);
        quat quat2;
        quat_from_axisangle(&quat2, &mms->axis, mms->rotation*0.001*(time-mms->last_time));
        quat_mult(&mms->rot, &quat2, &copyrot);
        mms->last_time = time;
    }

    b->rot = mms->rot;
    body_compute_modelmatrix(b);
}

static void addremove_mirrors(struct Scene *scene, unsigned num)
{
    static struct Shape *sphereshape = NULL;
    static struct MoveMirrorState *mm = NULL;
    if (!sphereshape) {
        sphereshape = create_sphere();
        mm = malloc(sizeof (struct MoveMirrorState));
        mm->last_time = 0;
        // rotation per second
        ASSIGN_VEC3(mm->axis, 0.0, 0.0, 1.0);
        mm->rotation = 2.0;
        // initial rotation
        vec3 init_axis;
        ASSIGN_VEC3(init_axis, 0.0, 1.0, 0.0);
        quat_from_axisangle(&mm->rot, &init_axis, 1.0);
    }

    if (num < 6) num = 6;
    if (scene->bodycount - scene->mirrorcount + num > MAX_BODIES) num = MAX_BODIES - (scene->bodycount - scene->mirrorcount);
    scene->bodycount -= scene->mirrorcount;

    vec3 rot_axis;
    ASSIGN_VEC3(rot_axis, 0.0, 1.0, 0.0);

    for (unsigned sp=0; sp<num; sp++) {
        scene->bodies[scene->bodycount+sp].shape = sphereshape;
        ASSIGN_VEC3(scene->bodies[scene->bodycount+sp].size, 0.7, 0.4, 0.9);
        ASSIGN_VEC3(scene->bodies[scene->bodycount+sp].center, 4.0*sin(2.0*M_PI*sp/num), 4.0*cos(2.0*M_PI*sp/num), 0.0);
        quat_from_axisangle(&scene->bodies[scene->bodycount+sp].rot, &rot_axis, 1.0);
        scene->bodies[scene->bodycount+sp].shader = scene->reflect;
        scene->bodies[scene->bodycount+sp].shader_in_reflection = scene->phong;
        sphere_color(&scene->bodies[scene->bodycount+sp].color, sp, num);
        scene->bodies[scene->bodycount+sp].move = move_mirror;
        scene->bodies[scene->bodycount+sp].move_state = mm;

        if (sp >= scene->mirrorcount) {
            //printf("create new cube texture for sphere %d\n", sp);
            glGenTextures(1, &scene->bodies[scene->bodycount+sp].reflection_texture);
            glBindTexture(GL_TEXTURE_CUBE_MAP, scene->bodies[scene->bodycount+sp].reflection_texture);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BASE_LEVEL, 0);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_LEVEL, 0); //TODO enable mipmapping later

            for (unsigned side=0; side<6; side++) {
                glTexImage2D(cube_map_targets[side],
                        0, // TODO other layers for mipmapping
                        GL_RGB,
                        REFL_TEX_SIZE,
                        REFL_TEX_SIZE,
                        0,
                        GL_RGB,
                        GL_UNSIGNED_BYTE,
                        0); // no data now, just allocate space
            }
        }
    }
    if (num < scene->mirrorcount) {
        for (unsigned sp=num; sp<scene->mirrorcount; sp++) {
            //printf("delete cube texture of sphere %d\n", sp);
            glDeleteTextures(1, &scene->bodies[scene->bodycount+sp].reflection_texture);
        }
    }
    scene->bodycount += num;
    scene->mirrorcount = num;
}

struct MoveBodyState {
    // position state
    GLfloat phi;
    GLfloat pos_offs;

    // rotation state
    vec3 axis;
    GLfloat rotation;

    unsigned last_time;
};
static void move_body(struct Body *b, unsigned time, void *s)
{
    struct MoveBodyState *state = (struct MoveBodyState *)s;

    // update rotation
    GLfloat angle = state->rotation*0.001*(time-state->last_time);
    body_apply_rotation(b, &state->axis, angle);

    // update position
    GLfloat r = 3.5 + 3.0*sin(0.001*time + state->pos_offs);
    b->center.v[0] = r*sin(state->phi);
    b->center.v[1] = r*cos(state->phi);
    b->center.v[2] = 2.0*sin(0.001*time + M_PI/2.0 + state->pos_offs);

    body_compute_modelmatrix(b);
        state->last_time = time;
}

static void move_light(vec3 *v, unsigned time, void *state)
{
    float *offs = state;
    v->v[0] = 2.0*sin(0.001*time + *offs);
    v->v[1] = 3.0*cos(0.001*time + *offs);
    v->v[2] = 3.0*sin(0.007*time + *offs);
}

static float frand(float max)
{
    return max*(float)rand()/(float)RAND_MAX;
}

static struct Scene *init_scene(void)
{
    struct Scene *ret = malloc(sizeof(struct Scene));

    init_shaders(ret);

    struct Shape *cubeshape = create_cube();

    vec3 rot_axis;

    ret->bodies[0].shape = cubeshape;
    ASSIGN_VEC3(ret->bodies[0].size, 1.0, 1.0, 1.0);
    ASSIGN_VEC3(ret->bodies[0].center, 0.0, 0.0, 0.0);
    ASSIGN_VEC3(rot_axis, 0.0, 1.0, 0.0);
    quat_from_axisangle(&ret->bodies[0].rot, &rot_axis, 0.0);
    ret->bodies[0].shader = ret->phong;
    ret->bodies[0].shader_in_reflection = NULL;
    ASSIGN_VEC3(ret->bodies[0].color, 1.0, 1.0, 1.0);
    ret->bodies[0].reflection_texture = 0;
    ret->bodies[0].move = NULL;
    ret->bodycount = 1;

    for (unsigned row=0; row<4; row++) {
        GLfloat phi = M_PI/4.0 + M_PI/2.0*row;
        for (unsigned item=0; item<6; item++) {
            ret->bodies[ret->bodycount].shape = cubeshape;
            ASSIGN_VEC3(ret->bodies[ret->bodycount].size, 0.1+frand(0.2), 0.1+frand(0.2), 0.1+frand(0.2));
            ASSIGN_VEC3(rot_axis, frand(4.0)-2.0, frand(4.0)-2.0, frand(4.0)-2.0);
            quat_from_axisangle(&ret->bodies[ret->bodycount].rot, &rot_axis, frand(4.0));

            struct MoveBodyState *rb = malloc(sizeof(struct MoveBodyState));
            rb->phi = phi;
            rb->pos_offs = 1.0*item;
            ASSIGN_VEC3(rb->axis, frand(4.0)-2.0, frand(4.0)-2.0, frand(4.0)-2.0);
            rb->rotation = frand(M_PI);
            rb->last_time = 0;

            ret->bodies[ret->bodycount].move_state = rb;
            ret->bodies[ret->bodycount].move = move_body;
            ret->bodies[ret->bodycount].move(&ret->bodies[ret->bodycount], 0, rb); // initial pos

            ret->bodies[ret->bodycount].shader = ret->phong;
            ret->bodies[ret->bodycount].shader_in_reflection = NULL;
            ret->bodies[ret->bodycount].reflection_texture = 0;
            ASSIGN_VEC3(ret->bodies[ret->bodycount].color, 1.0, 1.0, 1.0);

            ret->bodycount++;
        }
    }
    printf("Boxcount: %d\n", ret->bodycount);

    ret->mirrorcount = 0;
    addremove_mirrors(ret, 16);

    for (unsigned i=0; i<ret->bodycount; i++)
        body_compute_modelmatrix(&ret->bodies[i]);

    float *offs;
    ASSIGN_VEC3(ret->lights.color[0], 0.9, 0.4, 0.4);
    ret->lights.move[0] = move_light;
    offs = malloc(sizeof(float));
    *offs = 0.0;
    ret->lights.move_state[0] = offs;
    ret->lights.move[0](&ret->lights.pos[0], 0, ret->lights.move_state[0]); // initial pos
    ASSIGN_VEC3(ret->lights.color[1], 0.4, 0.4, 0.9);
    ret->lights.move[1] = move_light;
    offs = malloc(sizeof(float));
    *offs = M_PI;
    ret->lights.move_state[1] = offs;
    ret->lights.move[1](&ret->lights.pos[1], 0, ret->lights.move_state[1]); // initial pos

    ret->lights.count = 2;

    for (unsigned cam=0; cam<6; cam++) {
        ret->reflection_cameras[cam].near = 0.01;
        ret->reflection_cameras[cam].far = 50.0;
        ret->reflection_cameras[cam].left = -0.01;
        ret->reflection_cameras[cam].right = 0.01;
        ret->reflection_cameras[cam].bottom = -0.01;
        ret->reflection_cameras[cam].top = 0.01;
        frustum(&ret->reflection_cameras[cam].proj,
                ret->reflection_cameras[cam].left,
                ret->reflection_cameras[cam].right,
                ret->reflection_cameras[cam].bottom,
                ret->reflection_cameras[cam].top,
                ret->reflection_cameras[cam].near,
                ret->reflection_cameras[cam].far);
    }
#define COL 0.0
    ASSIGN_VEC4(ret->reflection_cameras[0].clearColor, 0.0, COL, 0.0, 1.0);
    ASSIGN_VEC4(ret->reflection_cameras[1].clearColor, 0.0, 0.0, COL, 1.0);
    ASSIGN_VEC4(ret->reflection_cameras[2].clearColor, COL, 0.0, 0.0, 1.0);
    ASSIGN_VEC4(ret->reflection_cameras[3].clearColor, 0.0, COL, COL, 1.0);
    ASSIGN_VEC4(ret->reflection_cameras[4].clearColor, COL, 0.0, COL, 1.0);
    ASSIGN_VEC4(ret->reflection_cameras[5].clearColor, COL, COL, 0.0, 1.0);
#undef COL

    glGenFramebuffers(1, &ret->reflection_fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, ret->reflection_fbo);
    GLuint depthrb;
    glGenRenderbuffers(1, &depthrb);
    glBindRenderbuffer(GL_RENDERBUFFER, depthrb);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, REFL_TEX_SIZE, REFL_TEX_SIZE);
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrb);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    ret->sprite_buffer = malloc(sizeof(struct Shape));
    glGenVertexArrays(1, &ret->sprite_buffer->vao);
    glBindVertexArray(ret->sprite_buffer->vao);
    glGenBuffers(1, &ret->sprite_buffer->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, ret->sprite_buffer->vbo);
    // vertex buffer is non-interleaved; vec4 position in eye space, vec3 color
    // this is just placeholder data, everything will be overwritten in draw_scene()
    glBufferData(GL_ARRAY_BUFFER, (sizeof(vec4)+sizeof(vec3))*MAX_LIGHTS, ret->lights.pos[0].v, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (void*)0); // Pos
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(vec4)*MAX_LIGHTS)); // Color
    glEnableVertexAttribArray(2);
    if (checkGLerror(__LINE__) != GL_NO_ERROR)
        exit(1);
    glBindVertexArray(0);

    return ret;
}

static struct Camera *init_camera(int width, int height)
{
    struct Camera *ret = malloc(sizeof(struct Camera));

    ASSIGN_VEC3(ret->pos, -4.0, 2.0, 3.0);
    ASSIGN_VEC3(ret->lookat, 0.0, 0.0, 0.0);
    ASSIGN_VEC3(ret->up, 0.0, 0.0, 1.0);
    camera_update_view(ret);

    GLfloat aspect = (GLfloat)width/height;
    ret->near = 0.1;
    ret->far = 50.0;
    ret->left = -0.1*aspect;
    ret->right = 0.1*aspect;
    ret->bottom = -0.1;
    ret->top = 0.1;
    frustum(&ret->proj,
            ret->left, ret->right,
            ret->bottom, ret->top,
            ret->near, ret->far);

    ASSIGN_VEC4(ret->clearColor, 0.0, 0.1, 0.3, 1.0);

    return ret;
}


/***************************************/
/* Main part                           */

static void draw_scene(struct Scene *scene, struct Camera *camera, bool in_reflection)
{
    //transform lights into eye space
    vec4 lpos_eye[MAX_LIGHTS];
    for (unsigned l=0; l<scene->lights.count; l++) {
        vec4 light_in_modelspace;
        vec4from3(&light_in_modelspace, &scene->lights.pos[l], 1.0);
        mat4vec4mult(&lpos_eye[l], &camera->view, &light_in_modelspace);
        //vec4fprintf(stdout, &lpos_eye[l], "Light");
    }

    // draw all bodies
    for (unsigned b=0; b<scene->bodycount; b++) {
        // simple culling: skip if the body is behind the camera
        vec3 fwd, bdy;
        vec3sub(&fwd, &camera->lookat, &camera->pos);
        vec3sub(&bdy, &scene->bodies[b].center, &camera->pos);
        if (vec3dot(&fwd, &bdy) < 0.0) continue;

        mat4 mv;
        mat3 mv3;
        mat4mult_opt(&mv, &camera->view, &scene->bodies[b].model);
        mat4topleft3(&mv3, &mv);

        struct Shader *body_shader = NULL;
        if (in_reflection && scene->bodies[b].shader_in_reflection) {
            body_shader = scene->bodies[b].shader_in_reflection;
            glUseProgram(body_shader->prog);
        } else {
            body_shader = scene->bodies[b].shader;
            glUseProgram(body_shader->prog);
            if (scene->bodies[b].reflection_texture) {
                glBindTexture(GL_TEXTURE_CUBE_MAP, scene->bodies[b].reflection_texture);
                //TODO multiple textures: glUniform1i(samplerloc, texunit)
                //TODO use sampler object from GL3.3?

                // R matrix for reflection transform: transpose of view matrix
                glUniformMatrix4fv(body_shader->r_loc, 1, GL_TRUE, camera->view.m);
            }
        }

        /*mat4fprintf(stderr, &scene->bodies[b].model, "model");
        mat4fprintf(stderr, &camera->view, "view");
        mat4fprintf(stderr, &mv, "modelview");
        mat4fprintf(stderr, &camera->proj, "proj");
        mat3fprintf(stderr, &mv3, "mv top 3x3");
        mat3fprintf(stderr, &mvi, "mv inverse");*/
        /*if (!in_reflection && scene->bodies[b].reflection_texture) {
            mat4fprintf(stderr, &mv, "modelview");
            mat3fprintf(stderr, &mvi, "mv inverse");
        }*/
        glUniformMatrix4fv(body_shader->mv_loc, 1, GL_FALSE, mv.m);
        glUniformMatrix4fv(body_shader->p_loc, 1, GL_FALSE, camera->proj.m);
        glUniform3fv(body_shader->scale_loc, 1, scene->bodies[b].size.v);

        glUniform4fv(body_shader->lpos_loc, scene->lights.count, lpos_eye[0].v);
        glUniform3fv(body_shader->lcolor_loc, scene->lights.count, scene->lights.color[0].v);
        glUniform1i(body_shader->lcount_loc, scene->lights.count);

        glUniform3fv(body_shader->bcolor_loc, 1, scene->bodies[b].color.v);

        glBindVertexArray(scene->bodies[b].shape->vao);
        glDrawElements(GL_TRIANGLES, scene->bodies[b].shape->indexcount, GL_UNSIGNED_INT, NULL);
    }
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    if (!in_reflection) {
        // draw point sprites at the lights
        glPointSize(5.0);
        glBindVertexArray(scene->sprite_buffer->vao);
        /*int size;
          glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
          printf("buffer size %d\n", size);*/
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec4)*MAX_LIGHTS, lpos_eye[0].v);
        //TODO the color of the lights don't change
        glBufferSubData(GL_ARRAY_BUFFER, sizeof(vec4)*MAX_LIGHTS, sizeof(vec3)*MAX_LIGHTS, &scene->lights.color[0].v);
        //TODO leave time between buffer update and draw
        //  (this small buffer is probably inlined into the GPU command stream though)
        glUseProgram(scene->sprite->prog);
        glUniformMatrix4fv(scene->sprite->p_loc, 1, GL_FALSE, camera->proj.m);
        glDrawArrays(GL_POINTS, 0, scene->lights.count);
    }
}

static void update_reflection_on(struct Body *b, struct Scene *scene)
{
    static const vec3 lookat_offset[] = {
        {{ 1.0,  0.0,  0.0}},
        {{-1.0,  0.0,  0.0}},
        {{ 0.0,  1.0,  0.0}},
        {{ 0.0, -1.0,  0.0}},
        {{ 0.0,  0.0,  1.0}},
        {{ 0.0,  0.0, -1.0}},
    };
    static const vec3 up_offset[] = {
        {{ 0.0, -1.0,  0.0}},
        {{ 0.0, -1.0,  0.0}},
        {{ 0.0,  0.0,  1.0}},
        {{ 0.0,  0.0, -1.0}},
        {{ 0.0, -1.0,  0.0}},
        {{ 0.0, -1.0,  0.0}},
    };

    // TODO multisampling: draw into an FBO with multisampled render target, blit that into the texture
    /* TODO layered rendering into the cube map: we need geometry shader
     *  glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, b->reflection_texture, 0);
     *  this attaches all 6 faces as layers 0..5
     *  in the GS the gl_Layer output specifies which layer the vertex should go to (need to set this for each vertex)
     *  we need to do the position/normal/light transforms in the GS
     *  we need to pass the matrices as uniform arrays
     *  separate clear color for the faces: we need to do it one-by-one with 2D FBO (ewww)
     *
     *  problem: GS is incredibly slow, because it limits GPU parallelism
     *          it transforms 3 vertices in a loop -> 3x slower
     *          I read somewhere that its output is a synchronization point -> need to wait for all threads
     */
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, scene->reflection_fbo);
    for (unsigned cam=0; cam<6; cam++) {
        vec3copy(&scene->reflection_cameras[cam].pos, &b->center);
        vec3add(&scene->reflection_cameras[cam].lookat, &b->center, &lookat_offset[cam]);
        vec3copy(&scene->reflection_cameras[cam].up, &up_offset[cam]);
        camera_update_view(&scene->reflection_cameras[cam]);

        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                cube_map_targets[cam], b->reflection_texture, 0);
        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE) {
            fprintf(stderr, "FBO not complete!  status = 0x%04x\n", status);
            exit(1);
        }

        glViewport(0, 0, REFL_TEX_SIZE, REFL_TEX_SIZE);
        glClearColor(scene->reflection_cameras[cam].clearColor.v[0], scene->reflection_cameras[cam].clearColor.v[1],
                scene->reflection_cameras[cam].clearColor.v[2], scene->reflection_cameras[cam].clearColor.v[3]);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        draw_scene(scene, &scene->reflection_cameras[cam], true);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                cube_map_targets[cam], 0, 0);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


static void redraw_window(struct Scene *scene, struct Camera *camera)
{
    for (unsigned b=0; b<scene->bodycount; b++) {
        if (scene->bodies[b].reflection_texture) {
            update_reflection_on(&scene->bodies[b], scene);
        }
    }

    glViewport(0, 0, camera->viewport_width, camera->viewport_height);
    glClearColor(camera->clearColor.v[0], camera->clearColor.v[1],
            camera->clearColor.v[2],camera->clearColor.v[3]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    draw_scene(scene, camera, false);
}

static void update_viewport(struct Camera *camera, int width, int height)
{
    camera->viewport_width = width;
    camera->viewport_height = height;

    GLfloat aspect = (GLfloat)width/height;
    camera->left = -0.1*aspect;
    camera->right = 0.1*aspect;
    frustum(&camera->proj,
            camera->left, camera->right,
            camera->bottom, camera->top,
            camera->near, camera->far);
}

static void mainloop(SDL_Window *window, struct Scene *scene, struct Camera *camera)
{
    unsigned last_time = 0;
    unsigned frames = 0;

    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    while (1) {
        unsigned time = SDL_GetTicks();

        if (time - last_time > 2000) {
            printf("Frames in 2 sec %d, %f fps\n", frames, 0.5*frames);
            last_time = time;
            frames = 0;
        }
        frames++;

        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    return;
                case SDL_WINDOWEVENT:
                    switch (event.window.event) {
                        case SDL_WINDOWEVENT_RESIZED:
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            update_viewport(camera, event.window.data1, event.window.data2);
                            break;
                    }
                    break;
                case SDL_KEYUP:
                    if (event.key.keysym.sym == SDLK_ESCAPE) {
                        return;
                    }
                    break;
                case SDL_KEYDOWN:
                    if (event.key.keysym.sym == SDLK_SPACE) {
                        //TODO stop-start animation (bodies AND lights)
                    } else if (event.key.keysym.sym == SDLK_LEFT) {
                        // one less mirror
                        addremove_mirrors(scene, scene->mirrorcount-1);
                        for (unsigned i=0; i<scene->bodycount; i++)
                            body_compute_modelmatrix(&scene->bodies[i]);
                    } else if (event.key.keysym.sym == SDLK_RIGHT) {
                        // one more mirror
                        addremove_mirrors(scene, scene->mirrorcount+1);
                        for (unsigned i=0; i<scene->bodycount; i++)
                            body_compute_modelmatrix(&scene->bodies[i]);
                    }
                    //TODO toggle reflection on-off (which key?)
                    break;
                case SDL_MOUSEMOTION:
                    if (event.motion.state == SDL_BUTTON_LMASK) {
                        GLfloat dphi = 0.009*event.motion.xrel;
                        GLfloat dthe = 0.009*event.motion.yrel;

                        vec3 v;
                        vec3sub(&v, &camera->pos, &camera->lookat);
                        GLfloat len = vec3len(&v);
                        GLfloat phi = atan2(v.v[1], v.v[0]);
                        GLfloat the = acos(v.v[2]/len);

                        phi -= dphi;
                        the -= dthe;
                        if (the > M_PI) the = M_PI-0.001;
                        if (the < 0.0) the = 0.001;
                        v.v[0] = len*sin(the)*cos(phi);
                        v.v[1] = len*sin(the)*sin(phi);
                        v.v[2] = len*cos(the);
                        vec3add(&camera->pos, &camera->lookat, &v);
                        camera_update_view(camera);
                    }
                    if (event.motion.state == SDL_BUTTON_RMASK) {
                        vec3 v, cpos;
                        vec3copy(&cpos, &camera->pos);
                        vec3sub(&v, &camera->pos, &camera->lookat);
                        GLfloat s = 0.005*event.motion.yrel;
                        vec3scale(&v, s);
                        vec3add(&camera->pos, &cpos, &v);
                        camera_update_view(camera);
                    }
                    break;

            }
        }

        // update bodies
        if (ENABLE_ANIMATION) {
            for (unsigned b=0; b<scene->bodycount; b++) {
                if (scene->bodies[b].move) {
                    scene->bodies[b].move(&scene->bodies[b], time, scene->bodies[b].move_state);
                }
            }
        }

        // update lights
        for (unsigned l=0; l<scene->lights.count; l++) {
            if (scene->lights.move[l]) {
                scene->lights.move[l](&scene->lights.pos[l], time, scene->lights.move_state[l]);
            }
        }

        redraw_window(scene, camera);
        SDL_GL_SwapWindow(window);
    }
}

int main(void)
{
    SDL_Window *mainwindow;
    SDL_GLContext maincontext;

    srand(42);

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "Unable to initialize SDL: %s\n", SDL_GetError());
        exit(1);
    }

    atexit(SDL_Quit);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    // this needs SDL 2.0.8
    SDL_SetHint(SDL_HINT_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR, "0");

    //SDL_EnableScreenSaver();

    SDL_GL_SetAttribute( SDL_GL_MULTISAMPLEBUFFERS, 1 );
    SDL_GL_SetAttribute( SDL_GL_MULTISAMPLESAMPLES, 4 );

    mainwindow = SDL_CreateWindow(program_name, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (!mainwindow) {
        SDL_GL_SetAttribute( SDL_GL_MULTISAMPLEBUFFERS, 0);
        SDL_GL_SetAttribute( SDL_GL_MULTISAMPLESAMPLES, 0);

        mainwindow = SDL_CreateWindow(program_name, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
        if (!mainwindow) {
            fprintf(stderr, "Unable to create window even without multisampling: %s\n", SDL_GetError());
            exit(1);
        }
    }

    maincontext = SDL_GL_CreateContext(mainwindow);
    if (!maincontext) {
        fprintf(stderr, "Unable to create GL context: %s\n", SDL_GetError());
        exit(1);
    }

    glewInit();

    int majorversion;
    int minorversion;
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &majorversion);
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &minorversion);
    int glmajor;
    int glminor;
    glGetIntegerv(GL_MAJOR_VERSION, &glmajor);
    glGetIntegerv(GL_MINOR_VERSION, &glminor);
    printf("Context version: SDLGL %d.%d GL %d.%d\n", majorversion, minorversion, glmajor, glminor);

    printf("Vendor: %s\nRenderer: %s\nVersion: %s\nGLSL Version: %s\n",
            glGetString(GL_VENDOR),
            glGetString(GL_RENDERER),
            glGetString(GL_VERSION),
            glGetString(GL_SHADING_LANGUAGE_VERSION));

    struct Scene *scene = init_scene();
    struct Camera *cam = init_camera(INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT);
    update_viewport(cam, INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT);

    printf(R"(
Usage:
    Cursor left: one less reflecting object
    Cursor right: one more reflecting object

    Hold left mouse button: move camera
    Hold right mouse button: zoom camera

    Esc: exit

)");

    mainloop(mainwindow, scene, cam);

    SDL_GL_DeleteContext(maincontext);
    SDL_DestroyWindow(mainwindow);
    SDL_Quit();

    return 0;
}

// vim: fdm=marker
